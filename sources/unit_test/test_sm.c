

#include <stdio.h>
#include <stdint.h>
#include "CUnit/Automated.h"
#include "smc_framework.h"
#include "log.h"


typedef struct sample_struct
{
  uint32_t check;
} sample_struct;

void trans_init_to_toto(sample_struct *context, void* arg)
{
  context->check++;
  DEBUG_LOG("in init to toto\n");
}

void trans_tata_to_toto(sample_struct *context, void* arg)
{
  context->check++;
  DEBUG_LOG("in tata to toto\n");
}

void trans_toto_to_tata(sample_struct *context, void* arg)
{
  context->check++;
  DEBUG_LOG("in toto to tata\n");
}

void trans_not_handled_event(sample_struct *context, void* arg)
{
  context->check = 0;
  DEBUG_LOG("not handled event\n");
  ERROR_LOG("Try the error %d\n", 156);
}

declare_events(sample, INIT_TO_TOTO, TOTO_TO_TATA, TATA_TO_TOTO);
declare_states(sample, INIT, TOTO, TATA);
declare_sm(sample, sample_struct,
                    /* STATE INIT */
                    {
                        /* EVENT INIT_TO_TOTO */
                        {trans_init_to_toto, TOTO},
                        /* EVENT TOTO_TO_TATA */
                        {trans_not_handled_event, INIT},
                        /* EVENT TATA_TO_TOTO */
                        {trans_not_handled_event, INIT},
                    },
                    /* STATE TOTO */
                    {
                        /* EVENT INIT_TO_TOTO */
                        {trans_not_handled_event, TOTO},
                        /* EVENT TOTO_TO_TATA */
                        {trans_toto_to_tata, TATA},
                        /* EVENT TATA_TO_TOTO */
                        {trans_not_handled_event, TOTO},
                    },
                    /* STATE TATA */
                    {
                        /* EVENT INIT_TO_TOTO */
                        {trans_not_handled_event, TATA},
                        /* EVENT TOTO_TO_TATA */
                        {trans_not_handled_event, TATA},
                        /* EVENT TATA_TO_TOTO */
                        {trans_tata_to_toto, TOTO},
                    });


void test()
{
  perform_event(sample, INIT_TO_TOTO, 0);
  CU_ASSERT(get_private_data_sm(sample).check == 1);
  perform_event(sample, INIT_TO_TOTO, 3);
  CU_ASSERT(get_private_data_sm(sample).check == 0);
  perform_event(sample, TOTO_TO_TATA, 0);
  CU_ASSERT(get_private_data_sm(sample).check == 1);
  perform_event(sample, TATA_TO_TOTO, 0);
  CU_ASSERT(get_private_data_sm(sample).check == 2);
  perform_event(sample, TATA_TO_TOTO, 0);
  CU_ASSERT(get_private_data_sm(sample).check == 0);
  perform_event(sample, TOTO_TO_TATA, 0);
  CU_ASSERT(get_private_data_sm(sample).check == 1);

}




/* The suite initialization function.
 * Opens the temporary file used by the tests.
 * Returns zero on success, non-zero otherwise.
 */
int init_suite1(void)
{
  get_private_data_sm(sample).check = 0;
  return 0;
}

/* The suite cleanup function.
 * Closes the temporary file used by the tests.
 * Returns zero on success, non-zero otherwise.
 */
int clean_suite1(void)
{
  return 0;
}


/* The main() function for setting up and running the tests.
 * Returns a CUE_SUCCESS on successful running, another
 * CUnit error code on failure.
 */
int main()
{
   CU_pSuite pSuite = NULL;

   /* initialize the CUnit test registry */
   if (CUE_SUCCESS != CU_initialize_registry())
      return CU_get_error();

   /* add a suite to the registry */
   pSuite = CU_add_suite("Suite_1", init_suite1, clean_suite1);
   if (NULL == pSuite) {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* add the tests to the suite */
   /* NOTE - ORDER IS IMPORTANT - MUST TEST fread() AFTER fprintf() */
   if ((NULL == CU_add_test(pSuite, "test 1rst call", test)) )
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* Run all tests using the CUnit Basic interface */
   CU_automated_run_tests();
   CU_cleanup_registry();
   return CU_get_error();
}
