/* smc_framework.h --
  version 0.0.1, Sept 10th, 2015

 Copyright (C) 2012 Stephen Kinger

 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.

 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:

 1. The origin of this software must not be misrepresented; you must not
 claim that you wrote the original software. If you use this software
 in a product, an acknowledgment in the product documentation would be
 appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
 misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

 Stephen Kinger stephen.kinger@gmail.com
 */

#ifndef __LOGGER_H__
#define __LOGGER_H__


#define DEBUG 1

#if (DEBUG == 1)
#define DEBUG_TEST 1
#else
#define DEBUG_TEST 0
#endif

#if (DEBUG_TEST == 1)

#define DEBUG_LOG(def, args...) \
        fprintf(stdout, "DEBUG %s:%d:%s(): " def, __FILE__, \
                                __LINE__, __func__, ##args)

#define ERROR_LOG(def, args...) \
        fprintf(stderr, "ERROR %s:%d:%s(): " def, __FILE__, \
                                __LINE__, __func__, ##args)
#else

#define DEBUG_LOG(...) /*Don't print*/
#define ERROR_LOG(...) /*Don't print*/

#endif                       

#endif /* __LOGGER_H__ */
