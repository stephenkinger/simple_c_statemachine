/* smc_framework.h --
  version 0.0.1, Aug 18th, 2012

 Copyright (C) 2012 Stephen Kinger

 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.

 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:

 1. The origin of this software must not be misrepresented; you must not
 claim that you wrote the original software. If you use this software
 in a product, an acknowledgment in the product documentation would be
 appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
 misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

 Stephen Kinger stephen.kinger@gmail.com
 */


#define declare_events(sm_name, ...) \
  typedef enum sm_name##events_e     \
  {                                  \
    __VA_ARGS__,                     \
    sm_name##events_count            \
  } sm_name##events_e


#define declare_states(sm_name, ...) \
  typedef enum sm_name##states_e     \
  {                                  \
    __VA_ARGS__,                     \
    sm_name##states_count            \
  } sm_name##states_e


#define set_state_sm(sm_name, state)                  \
    sm_name##_sm.current_state = state

#define get_private_data_sm(sm_name)                  \
    sm_name##_sm.private_data

#define declare_sm(sm_name, private_type, ...)                  \
  typedef struct sm_name##_context_t                            \
  {                                                             \
    private_type private_data;                                  \
    sm_name##states_e current_state;                            \
  } sm_name##_context_t;                                         \
                                                                \
  typedef struct sm_name##_transition_t                         \
  {                                                             \
    void (*handler)(private_type*, void*);                      \
    sm_name##states_e next_state;                               \
  } sm_name##_transition_t;                                     \
  sm_name##_transition_t                                        \
    sm_name##_table[sm_name##states_count][sm_name##events_count] = \
    {__VA_ARGS__};                                              \
  sm_name##_context_t sm_name##_sm = {.current_state = 0};                             \
  void sm_name##perform_event(sm_name##events_e event, void* param)\
  {                                                             \
    sm_name##_table[sm_name##_sm.current_state][event].handler(&sm_name##_sm.private_data, param);      \
    set_state_sm(sm_name, sm_name##_table[sm_name##_sm.current_state][event].next_state);\
  }                                                              \


#define perform_event(sm_name, event, param)                    \
    sm_name##perform_event(event, (void*)param)


